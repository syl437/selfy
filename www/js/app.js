// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','starter.factories'])

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(true);
})

.run(function($ionicPlatform,$rootScope,$http,DealsFactory) {
	$rootScope.Host = "http://www.tapper.co.il/selfy/php/";
	$rootScope.Data = null;
	$rootScope.categories = [];
	$rootScope.suppliers = [];
	$rootScope.SupplierData = [];
	$rootScope.deals = [];
	$rootScope.USER = [];
	$rootScope.MainJson = [];
	$rootScope.PhotoGrapherImages = [];
	$rootScope.ImageUrl = '';
	$rootScope.EventId = '';
	$rootScope.EventData = '';
	$rootScope.eventImage = '';
	/*
	DealsFactory.getData().then(function(data)
	{
		DealsFactory.getDeals();
	});
	*/
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
	
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })


	
	.state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
	
	.state('app.imagepreview', {
      url: '/imagepreview',
      views: {
        'menuContent': {
          templateUrl: 'templates/imagepreview.html',
          controller: 'ImagePreviewCtrl'
        }
      }
    })
	
	.state('app.event', {
      url: '/event',
      views: {
        'menuContent': {
          templateUrl: 'templates/event.html',
          controller: 'eventCtrl'
        }
      }
    })
	
	.state('app.imagecomments', {
      url: '/imagecomments/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/imagecomments.html',
          controller: 'ImageCommentsCtrl'
        }
      }
    })
	
	
		.state('app.supplierdetails', {
      url: '/supplierdetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'SupplierDetailsCtrl'
        }
      }
    })
	
		.state('app.share', {
      url: '/share',
      views: {
        'menuContent': {
          templateUrl: 'templates/share.html',
          controller: 'ShareEventCtrl'
        }
      }
    })
	
		.state('app.photographer', {
      url: '/photographer',
      views: {
        'menuContent': {
          templateUrl: 'templates/photographer.html',
          controller: 'PhotoGrapherCtrl'
        }
      }
    });	
	
	

	

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
