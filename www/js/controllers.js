angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,DealsFactory,$rootScope,$cordovaBarcodeScanner,$cordovaSocialSharing) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  SupplierIndex = 2;
  $scope.sideMenuWidth = window.innerWidth*0.8;
  $rootScope.$watch('eventImage', function()
  {
		$scope.eventImage = $rootScope.eventImage;
		//alert($scope.eventImage) 
  });			
  
  /*
  DealsFactory.getCustomerById().then(function(data){
			console.log("getCustomerById")
  });
  */
  
  $scope.scanCode = function()
  {
	  cordova.plugins.barcodeScanner.scan(
	  function (result) 
	  {
	//	  if (result.text)
	//	  {
			$scope.Split = result.text.split("-");
			$scope.newString = result.text.replace("selfy-", "");
			if ($scope.Split[0] == "selfy")
			{
				if ($scope.newString)
				{
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					send_data = 
					{
						'password' : $scope.newString
					};

					$http.post($rootScope.Host+'/login.php', send_data)
					.success(function(data, status, headers, config)
					{


						if (data[0].status == 0)
						{
							$ionicPopup.alert({
							title: 'סיסמה שגויה יש לנסות שוב',
							buttons: [{
								text: 'OK',
								type: 'button-positive',
							  }]
						   });						
						}
						else
						{
							$rootScope.MainJson = data;
							$localStorage.eventid = String(data[0].index);
							$localStorage.supplierindex = data[0].supplier_index;
							$localStorage.eventpassword = data[0].app_pass;
							$state.go('app.event');
						}

						
					})
					.error(function(data, status, headers, config)
					{

					});						
				}


				
			}
			else
			{
				$ionicPopup.alert({
				title: 'קוד QR לא מוכר , יש לנסות שוב',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });
			   
			}
			

			  
	//	  }
		  /*
		  alert("We got a barcode\n" +
				"Result: " + result.text + "\n" +
				"Format: " + result.format + "\n" +
				"Cancelled: " + result.cancelled);
		*/
	  }, 
	  function (error) 
	  {
		  
	  }
   );
  }
  
  
  $scope.ShareOptions = function()
  {
	  $cordovaSocialSharing
	.share('test', 'test', '', 'http://ynet.co.il') // Share via native share sheet
	.then(function(result) {
	  alert (result);
	}, function(err) {
		//alert (err);
	  // An error occured. Show a message to the user
	});
  }
  
  
})


.controller('eventCtrl', function($scope,$rootScope,$stateParams,DealsFactory,$timeout,$ionicPopup,$http,$state,$localStorage) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
			 $scope.host = $rootScope.Host;
			 
			 
			 DealsFactory.getEventData().then(function(data)
			 {
				$scope.EventData = data;
				
				console.log($scope.EventData)
				$scope.eventImage = $scope.host + 'uploads/YBL_3416.JPG';
				$rootScope.eventImage = $scope.eventImage;
  			 });

			 DealsFactory.getSupplierInfo().then(function(data)
			 { 
			   $scope.SupplierDetails = data[0];
			   //alert ($scope.SupplierImages)
				console.log("MainData : " ,data)
			 });

		 
			 $scope.gotoMain = function()
			 {
				 $state.go('app.main');
			 }
 		 }, 200);
	}); 
})
 
			
			
.controller('MainCtrl', function($scope,DealsFactory,$rootScope,$localStorage,$timeout,$ionicModal,$cordovaCamera,$http,$ionicPopup) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
	 	 {
			$scope.navTitle='<img class="title-image" src="img/head_logo.png" />';
			$scope.host = $rootScope.Host;
			$scope.CameraImageSrc = $rootScope.ImageUrl;
			//
			$rootScope.eventImage = $scope.host + 'uploads/YBL_3416.JPG';
			console.log("Id : " , $localStorage.eventid)
			 DealsFactory.getData().then(function(data)
			 {
				$scope.Images = data;
				$rootScope.MainJson = data;
				console.log("MainData : " ,data)
  			 });
  
			$rootScope.$watch('ImageUrl', function()
			{
				$scope.CameraImageSrc = $rootScope.ImageUrl;
			});			
			
			$scope.preview = 
			{
				"text" : ""
			}




	
			$scope.ImageModal = function(imageSrc)
			{
				$scope.imageSrc = imageSrc;
			   $ionicModal.fromTemplateUrl('image-modal.html', {
				  scope: $scope,
				  animation: 'slide-in-up'
				}).then(function(modal) {
				  $scope.modal = modal;
				   $scope.modal.show();
				});
			}	
			$scope.closeModal = function()
			{
				$scope.modal.hide();
			}
			
			$scope.TakePicture = function()
			{
				/*$ionicModal.fromTemplateUrl('templates/preview_modal.html', {
							  scope: $scope,
							  animation: 'slide-in-up'
						}).then(function(imagePreview) {
							  $scope.imagePreview = imagePreview;
							   $scope.imagePreview.show();

						});		*/		
				
				options = { 
					quality : 75, 
					destinationType : Camera.DestinationType.FILE_URI, 
					sourceType : Camera.PictureSourceType.CAMERA, 
					allowEdit : false,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 600,
					targetHeight: 600,
					popoverOptions: CameraPopoverOptions,
					saveToPhotoAlbum: false,
					correctOrientation: true
				};
				
			     $cordovaCamera.getPicture(options).then(function(imageData) 
				{
					$scope.imgURI = imageData
					var myImg = $scope.imgURI;
					var options = new FileUploadOptions();
					options.mimeType = 'jpeg';
					options.fileKey = "file";
					options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
					//alert(options.fileName)
					var params = {};
					//params.user_token = localStorage.getItem('auth_token');
					//params.user_email = localStorage.getItem('email');
					options.params = params;
					var ft = new FileTransfer();
					ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
			});
			
			$scope.onUploadSuccess = function(data)
			{
				$timeout(function() {

					if (data.response) 
					{
						$scope.responseimage = data.response;
						$rootScope.ImageUrl = data.response;
				
						$ionicModal.fromTemplateUrl('templates/preview_modal.html', {
							  scope: $scope,
							  animation: 'slide-in-up'
						}).then(function(imagePreview) {
							  $scope.imagePreview = imagePreview;
							   $scope.imagePreview.show();

						});							
					}
					
				}, 300);
			}
			
			$scope.onUploadFail = function(data)
			{
				alert (data);
			}
			
			
			$scope.closePreview = function()
			{
				$scope.imagePreview.hide();
			}
			
			$scope.sendImage = function()
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				
				send_data = 
				{
					'event' : $localStorage.eventid,
					'message' : $scope.preview.text,
					'image' : $rootScope.ImageUrl
				};

				$scope.now = new Date(); 		
			    $scope.month = $scope.now.getMonth()+1;
			    $scope.month = ("0" + $scope.month).substr(-2);  
				$scope.day = ("0" + $scope.now.getDay()).substr(-2);
				$scope.then = $scope.now.getFullYear()+'-'+$scope.month+'-'+$scope.day; 
				$scope.hours = $scope.now.getHours();
				$scope.minutes = ("0" + $scope.now.getMinutes()).substr(-2);
				$scope.seconds  = ("0" + $scope.now.getSeconds()).substr(-2);
				$scope.newdate = $scope.then+ ' ' + $scope.hours+':'+ $scope.minutes+ ':' + $scope.seconds;

				
				$scope.Images.unshift({
					"likes": "0",
					"comments": "0",
					"image": $rootScope.ImageUrl,
					"text": $scope.preview.text,
					"date" :  $scope.newdate
				});

			
				$http.post($rootScope.Host+'/send_photo.php', send_data)
				.success(function(data, status, headers, config)
				{	
				})
				.error(function(data, status, headers, config)
				{

				});	

				$scope.imagePreview.hide();				
			}

		
		}
		
		$scope.likeImage = function(index)
		{
			$scope.LikedImage = $scope.Images[index];
			$scope.ImageIndex = $scope.LikedImage.index;

			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_data = 
			{
				'image_id' : $scope.ImageIndex,
				'event_id' : $localStorage.eventid,
				'user_id' : $localStorage.userid
			};

			$http.post($rootScope.Host+'/like_image.php', send_data)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$scope.LikedImage.likes++;
				}
				else
				{
					$ionicPopup.alert({
					title: 'אי אפשר לעשות לייק לתמונה פעמיים',
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });	
			   }

			})
			.error(function(data, status, headers, config)
			{

			});				
		}
		
		$scope.supplierInfo = function()
		{
			$state.go('app.event');
		}

		
 	 }, 200);
	}); 
})



.controller('LoginCtrl', function($scope,$rootScope,$stateParams,DealsFactory,$timeout,$ionicPopup,$http,$state,$localStorage,$cordovaBarcodeScanner) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
			$scope.host = $rootScope.Host;
			
		$scope.login = 
		{
			"password" : "4242"
		}
			
		$scope.LoginBtn = function()
		{
			if ($scope.login.password =="")
			{
				$ionicPopup.alert({
				title: 'יש להזין סיסמה',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}
			else
			{
				$scope.doLogin($scope.login.password)
			}
		}	

		$scope.doLogin = function(appkey)
		{
			if (appkey)
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_data = 
				{
					'password' : appkey
				};

				$http.post($rootScope.Host+'/login.php', send_data)
				.success(function(data, status, headers, config)
				{


					if (data[0].status == 0)
					{
						$ionicPopup.alert({
						title: 'סיסמה שגויה יש לנסות שוב',
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });						
					}
					else
					{
						$rootScope.MainJson = data;
						$localStorage.eventid = String(data[0].index);
						$localStorage.supplierindex = data[0].supplier_index;
						$localStorage.eventpassword = data[0].app_pass;
						$state.go('app.event');
					}

					
				})
				.error(function(data, status, headers, config)
				{

				});					
			}
			
		}

		$scope.scanQR = function()
		{
			  cordova.plugins.barcodeScanner.scan(
			  function (result) 
			  {
			//	  if (result.text)
			//	  {
					$scope.Split = result.text.split("-");
					$scope.newString = result.text.replace("selfy-", "");
					if ($scope.Split[0] == "selfy")
					{
						$scope.doLogin ($scope.newString);
					}
					else
					{
						$ionicPopup.alert({
						title: 'קוד QR לא מוכר , יש לנסות שוב',
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
					   
					}
					

					  
			//	  }
				  /*
				  alert("We got a barcode\n" +
						"Result: " + result.text + "\n" +
						"Format: " + result.format + "\n" +
						"Cancelled: " + result.cancelled);
				*/
			  }, 
			  function (error) 
			  {
				  
			  }
		   );
		}
			
 		 }, 200);
	}); 
})

 
.controller('ImagePreviewCtrl', function($scope,$rootScope,$stateParams,$timeout,$ionicPopup,$http,$state,$localStorage) 
{
	$scope.host = $rootScope.Host;
	//alert ($rootScope.ImageUrl);
	
})


.controller('ImageCommentsCtrl', function($scope,$rootScope,$stateParams,$timeout,$ionicPopup,$http,$state,$localStorage) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		 $timeout(function() 
		 {
				$scope.host = $rootScope.Host;
				$scope.Comments = [];
				$scope.params =
				{
					"text" : ""
				}
				$scope.Id = $stateParams.ItemId;
				$scope.Card = $rootScope.MainJson[$scope.Id];
				$scope.ImageIndex = $scope.Card.index;
				$scope.Comments = $rootScope.MainJson[$scope.Id].comments;
				console.log($scope.Comments);
				
				
				$scope.SendComment = function()
				{
				
				if ($scope.params.text)
				{
					
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
					$scope.now = new Date(); 		
					$scope.month = $scope.now.getMonth()+1;
					$scope.month = ("0" + $scope.month).substr(-2);  
					$scope.day = ("0" + $scope.now.getDay()).substr(-2);
					$scope.then = $scope.now.getFullYear()+'-'+$scope.month+'-'+$scope.day; 
					$scope.hours = $scope.now.getHours();
					$scope.minutes = ("0" + $scope.now.getMinutes()).substr(-2);
					$scope.seconds  = ("0" + $scope.now.getSeconds()).substr(-2);
					$scope.newdate = $scope.then+ ' ' + $scope.hours+':'+ $scope.minutes+ ':' + $scope.seconds;	
			
					send_data = 
					{
						'id' : $scope.ImageIndex,
						'event' : $localStorage.eventid,
						'message' : $scope.params.text,
					};
			
			
				
					$http.post($rootScope.Host+'/send_comment.php', send_data)
					.success(function(data, status, headers, config)
					{	
					})
					.error(function(data, status, headers, config)
					{
			
					});	
			
							
					$scope.Comments.unshift({
						"text": $scope.params.text,
					//	"date" : $scope.newdate
			
					});	
					
					
					$scope.params.text = '';		
				}
			
				}
				
				$scope.enterPress = function(keyEvent)
				{
					if (keyEvent.which === 13)
					{
						$scope.SendComment();
					}
				}

	
		}, 200);
	})
})


.controller('SupplierDetailsCtrl', function($scope,$rootScope,$stateParams,$timeout,$ionicPopup,$http,$state,$localStorage,DealsFactory,$ionicModal) 
{
		$scope.host = $rootScope.Host;
		$scope.contact = 
		{
			"name" : "",
			"phone" : "",
			"mail" : "",
			"desc" : ""
		}
		 DealsFactory.getSupplierInfo().then(function(data)
		 { 
		   $scope.SupplierDetails = data[0];
		   $scope.SupplierImages = $scope.SupplierDetails.supplierimages;
		   //alert ($scope.SupplierImages)
			console.log("MainData : " ,data)
		 });
		 
		 $scope.dialPhone = function()
		 {
			 if ($scope.SupplierDetails.phone)
			 {
				 window.location.href = $scope.SupplierDetails.phone;

			 }
		 }
		 
		 $scope.Video = function()
		 {
			 if ($scope.SupplierDetails.youtube)
			 {
				 window.location.href = $scope.SupplierDetails.youtube;

			 }
		}
		 
		 $scope.ContactSupplier = function()
		 {
			$ionicModal.fromTemplateUrl('templates/contact-supplier.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(modal) {
			  $scope.modal = modal;
			   $scope.modal.show();
			});
		 }
		 
		 $scope.closeModal = function()
		 {
			 $scope.modal.hide();
		 }
		 
		 $scope.sendContact = function()
		 {
			 
			if ($scope.contact.name =="")
			{
				$ionicPopup.alert({
				title: 'יש למלא שם מלא',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}
			else if ($scope.contact.phone =="" && $scope.contact.mail =="")
			{
				$ionicPopup.alert({
				title: 'יש למלא טלפון או מייל',
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}

			else
			{
				 $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					send_data = 
					{
						'supplier' : $localStorage.supplierindex,
						'name' : $scope.contact.name,
						'phone' : $scope.contact.phone,
						'email' : $scope.contact.mail,
						'desc' : $scope.contact.desc,
						'send' : 1
					};


				
					$http.post($rootScope.Host+'/send_contact.php', send_data)
					.success(function(data, status, headers, config)
					{	
					})
					.error(function(data, status, headers, config)
					{

					});	

					
					$ionicPopup.alert({
					title: 'תודה , פרטיך התקבלו בהצלחה',
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });	

						   
					$scope.contact.name = '';
					$scope.contact.phone = '';	
					$scope.contact.mail = '';	
					$scope.contact.desc = '';					
					$scope.modal.hide();				
			}

					
		 }
		 
		 //
			 
			 
})

.controller('ShareEventCtrl', function($scope,$rootScope,$stateParams,$timeout,$ionicPopup,$http,$state,$localStorage,DealsFactory,$ionicModal) 
{
	$scope.qrCode = "http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=selfy-"+$localStorage.eventpassword+"&chld=H|0";
})

.controller('PhotoGrapherCtrl', function($scope,$rootScope,$stateParams,$timeout,$ionicPopup,$http,$state,$localStorage,DealsFactory,$ionicModal) 
{
	
		$scope.host = $rootScope.Host;
		
		 DealsFactory.getPhotoGrapherImages().then(function(data)
		 { 
		 
		 $scope.Photographs = data;
		
		   //alert ($scope.SupplierImages)
			console.log("PhotoGrapher Images : " ,data)
		 });
		 
		 $scope.showModal = function(imageSrc)
		 {
				$scope.imageSrc = $scope.host+imageSrc;
			   $ionicModal.fromTemplateUrl('image-modal.html', {
				  scope: $scope,
				  animation: 'slide-in-up'
				}).then(function(modal) {
				  $scope.modal = modal;
				   $scope.modal.show();
				});			 
		 }
		 
		 $scope.closeModal = function()
		 {
			 $scope.modal.hide();
		 }
		 
		 

})


.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})


.filter('newDate', function () {
    return function (value) 
	{
		if(value)
		{
			var split = value.split(" ");
			var newdate = new Date(split[0]);
			var date = newdate.toDateString();
			var hours = split[1];
		}	return date+' ' + hours;
    };
});



