angular.module('starter.factories', [])


.factory('DealsFactory', function($http,$rootScope,$localStorage) {
	var data = [];
	var deals = [];
	var categories = [];
	var suppliers = [];
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	return {
		getEventData: function()
		{
			return $http.get($rootScope.Host+'/getEventData.php?event_id='+$localStorage.eventid).then(function(resp)
			{
				data = resp.data;
				$rootScope.EventData = data;
				return data;
			});
		},
		getData: function()
		{
			return $http.get($rootScope.Host+'/getData.php?event_id='+$localStorage.eventid).then(function(resp)
			{
				console.log("Resp : " , resp)
				data = resp.data;
				$rootScope.Data = data;
				return data;
			});
		},
		
		getSupplierInfo: function()
		{
			return $http.get($rootScope.Host+'/getSupplierInfo.php?supplier_id='+$localStorage.supplierindex).then(function(resp)
			{
			//	console.log("Resp : " , resp)
				data = resp.data;
				$rootScope.SupplierData = data;
				return data;
			});
		},
		getPhotoGrapherImages: function()
		{
			return $http.get($rootScope.Host+'/getPhotographerImages.php?supplier_Id='+$localStorage.supplierindex).then(function(resp)
			{
			//	console.log("Resp : " , resp)
				data = resp.data;
				$rootScope.PhotoGrapherImages = data;
				return data;
			});
		},		
		getDeals: function()
		{
		  	angular.forEach(data, function(Categories) 
			{
				categories.push(Categories);
				
				angular.forEach(Categories.suppliers, function(supplier) 
				{
					suppliers.push(supplier);
					
					angular.forEach(supplier.deals, function(deal) 
					{
						deals.push(deal);
					});
				});
			});
			
			$rootScope.categories = categories;
			$rootScope.suppliers = suppliers;
			$rootScope.deals = deals;
		},
		getSupplierNameById:function(supplierId)
		{
			var Name = "";
			angular.forEach($rootScope.suppliers, function(supplier) 
			{
				if(supplier.id == supplierId)
				{
					Name = supplier.name;
				}
			});
			return Name;
		},
		getDealById:function(dealId)
		{
			var Deal = "";
			angular.forEach($rootScope.deals, function(deal) 
			{
				if(deal.index == dealId)
				{
					Deal = deal;
				}
			});
			return Deal;
		},
		getCustomerById: function(ID)
		{
			return $http.get($rootScope.Host+'/get_customers_by_id.php?id=2').then(function(resp)
			{
				data = resp;
				$rootScope.USER = resp.data;
			});
		},
	}
})